'use strict'
import 'bootstrap-4-grid'
import 'normalize.css'
import './scss/main.scss'
import $ from 'jquery'
import Swiper from 'swiper'

// on scroll add class for header
window.addEventListener('scroll', () => {
  const header = document.querySelector('header')
  if (window.innerWidth >= 768) {
    const scroll = window.scrollY
    if (scroll >= 2) {
      header.classList.add('sticky')
    } else {
      header.classList.remove('sticky')
    }
  }
})

// cursor
document.addEventListener('DOMContentLoaded', () => {
  const $bigBall = document.querySelector('.cursor__ball--big')
  const $smallBall = document.querySelector('.cursor__ball--small')
  const $hoverables = document.querySelectorAll('.hoverable')

  // Listeners
  document.body.addEventListener('mousemove', onMouseMove)
  for (let i = 0; i < $hoverables.length; i++) {
    $hoverables[i].addEventListener('mouseenter', onMouseHover)
    $hoverables[i].addEventListener('mouseleave', onMouseHoverOut)
  }

  // Move the cursor
  function onMouseMove(e) {
    TweenMax.to($bigBall, 0.4, {
      x: e.clientX - 15,
      y: e.clientY - 15
    })
    TweenMax.to($smallBall, 0.1, {
      x: e.clientX - 5,
      y: e.clientY - 7
    })
  }

  // Hover an element
  function onMouseHover() {
    TweenMax.to($bigBall, 0.3, {
      scale: 4
    })
  }

  function onMouseHoverOut() {
    TweenMax.to($bigBall, 0.3, {
      scale: 1
    })
  }
})
// end cursor

// swiper
function swiperTours () {
  const swiper = new Swiper('#skin.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 30
  })
}
swiperTours()
// end swiper

// close hero conteriner
window.addEventListener('scroll', (e) => {
  // const emptyCont = document.querySelector('.hero-empty-container')
  const cont = document.querySelector('.hero .container')
  const scroll = window.scrollY

  if (scroll <= (window.innerHeight - 50)) {
    // emptyCont.style.height = `${scroll}px`
    cont.style.height = (window.innerHeight - scroll) + 'px'
  }
})
// end close hero conteriner

document.addEventListener('DOMContentLoaded', () => {
  const controller = new ScrollMagic.Controller()

  // intro animation when page are loaded
  // const intro = TweenMax.from('.hero__title', 1, {
  //   yPercent: 100,
  //   opacity: 0,
  //   ease: Cubic.easeOut
  // })

  // screen 1
  //  intro animation when start scroll
  const introTl = new TimelineMax()

  introTl
    .to('.hero__float-image', 1, {
      maxHeight: '0px',
      ease: Expo.easeInOut
    })
    .to('.title-wrap-item', 1, {
      top: '-100px',
      ease: Expo.easeInOut
    }, '0')

  // scene 1
  const scene1 = new ScrollMagic.Scene({
    triggerElement: '.trigger1',
    triggerHook: 'onLeave',
    duration: '100%'
  })
    .setTween(introTl)
    .addIndicators()
    .addTo(controller)

  //   //  2 screen
  //   let sliderTl = new TimelineMax()
  //
  //   sliderTl
  //     .from('.info-slider__title', 1, {
  //       opacity: 0,
  //       xPercent: -100,
  //       scale: 0.8,
  //       ease: Expo.easeInOut
  //     })
  //     .from('.slider-wrap', 1, {
  //       opacity: 0,
  //       yPercent: 50,
  //       ease: Power4.easeInOut
  //     }, '0')
  //     .from('.info-slider__btn', 1, {
  //       opacity: 0,
  //       yPercent: 20,
  //       scale: 0.95,
  //       ease: Expo.easeInOut
  //     }, '0.5');
  //
  //   // scene 2
  //   let scene2 = new ScrollMagic.Scene({
  //     triggerElement: '.trigger2',
  //     triggerHook: '0.5',
  //     duration: '80%'
  //   })
  //   .setTween(sliderTl)
  //   .addIndicators()
  //   // .setPin('.trigger2')
  //   .addTo(controller)

  //  3 screen
  const blogTl = new TimelineMax()
  blogTl
    .from('.blog-text__title', 0.5, {
      opacity: 0,
      yPercent: 10,
      ease: Expo.easeInOut
    })
    .from('.blog-text__description', 0.5, {
      opacity: 0,
      yPercent: 5,
      ease: Power4.easeInOut
    }, '0.1')
    .from('.blog__btn', 0.5, {
      opacity: 0,
      minHeight: 0,
      yPercent: 5,
      ease: Expo.easeInOut
    }, '0.2')
    .from('.blog__image.img-1', 0.5, {
      opacity: 0.5,
      minHeight: 0,
      yPercent: 5,
      ease: Expo.easeInOut
    }, '0')
    .to('.blog__image.img-1', 0.5, {
      yPercent: -20,
      ease: Expo.easeInOut
    }, '0.4')
    .from('.blog__image.img-2', 0.5, {
      // opacity: 0,
      minHeight: 0,
      yPercent: 10,
      ease: Expo.easeInOut
    }, '0.1')
    .to('.blog__image.img-2', 0.5, {
      yPercent: -10,
      ease: Expo.easeInOut
    }, '0.5')
    .from('.blog__image.img-3', 0.5, {
      // opacity: 0,
      minHeight: 0,
      yPercent: 15,
      ease: Expo.easeInOut
    }, '0.2')
    .to('.blog__image.img-3', 0.5, {
      yPercent: -10,
      ease: Expo.easeInOut
    }, '0.6')

  // scene 3
  const scene3 = new ScrollMagic.Scene({
    triggerElement: '.trigger3',
    triggerHook: '.6',
    duration: '120%'
  })
    .setTween(blogTl)
    .addIndicators()
    .addTo(controller)

  //   //  4 screen
  //   let infoText2Tl = new TimelineMax()
  //   infoText2Tl
  //     .from('.info-text__title', 1, {
  //       opacity: 0,
  //       yPercent: 100,
  //       scale: 0.8,
  //       ease: Expo.easeInOut
  //     })
  //     .from('.info-text__description', 2, {
  //       opacity: 0,
  //       yPercent: 50,
  //       ease: Power4.easeInOut
  //     }, '0.2');
  //
  //   // scene 4
  //   let scene4 = new ScrollMagic.Scene({
  //     triggerElement: '.trigger4',
  //     triggerHook: '1',
  //     duration: '100%'
  //   })
  //     .setTween(infoText2Tl)
  //     .addIndicators()
  //     .addTo(controller)
  //
  //
  //   //  5 screen
  //   let blog2Tl = new TimelineMax()
  //   blog2Tl
  //     .from('.blog2-text__title', 1, {
  //       opacity: 0,
  //       xPercent: 100,
  //       scale: 0.8,
  //       ease: Expo.easeInOut
  //     })
  //     .from('.blog2-text__description', 1, {
  //       opacity: 0,
  //       xPercent: 50,
  //       ease: Power4.easeInOut
  //     }, '0.2')
  //     .from('.blog2-btn', 1, {
  //       opacity: 0,
  //       yPercent: 20,
  //       ease: Power4.easeInOut
  //     }, '0.2');
  //
  //   // scene 5
  //   let scene5 = new ScrollMagic.Scene({
  //     triggerElement: '.trigger5',
  //     triggerHook: '1',
  //     duration: '100%'
  //   })
  //     .setTween(blog2Tl)
  //     .addIndicators()
  //     .addTo(controller)
})

// smooth scroll
const body = document.body
const jsScroll = document.getElementsByClassName('js-scroll')[0]
const height = jsScroll.getBoundingClientRect().height - 1
const speed = 0.05

var offset = 0
body.style.height = Math.floor(height) + 'px'

function smoothScroll () {
  offset += (window.pageYOffset - offset) * speed
  var scroll = 'translateY(-' + offset + 'px) translateZ(0)'
  jsScroll.style.transform = scroll
  requestAnimationFrame(smoothScroll)
}

smoothScroll()
